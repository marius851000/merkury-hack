import os
import codecs
from struct import *
from merkurytool import bytesToInt, bytesToString

_LONG = 0x4
_STRLEN = 0x100

# here a the content you should modidify
# all path can be relative
fichier = "TLS:ANB-US-GC/data_gc.rkv" # the rkv2 file ( end with .rkv. There are multiple rkv format ( for exemple, a compressed one in the Legend of Guardians:the owl of ga'hoole))
dest = "data" # the destination folder. This script will overwrite file in this folder. It shoudln't contain space


def extractRKV2(source,dest):
    if not os.path.isdir(dest):
        print("creating the "+dest+" folder")
        os.mkdir(dest)

    print("opening the file "+source)
    f = open(source,"rb")
    assert f.read(4) == b"RKV2"

    print("reading initial data")
    FILES = bytesToInt(f.read(_LONG))
    NAME_SIZE = bytesToInt(f.read(_LONG))
    FULLNAME_FILES = bytesToInt(f.read(_LONG))
    DUMMY1 = bytesToInt(f.read(_LONG))
    INFO_OFF = bytesToInt(f.read(_LONG))
    DUMMY2 = bytesToInt(f.read(_LONG))

    NAME_OFF = FILES * 20 + INFO_OFF
    INFO2_OFF = NAME_OFF+NAME_SIZE
    FULLNAME_OFF = FILES*16+INFO2_OFF

    for i in range(FILES):
        f.seek(INFO_OFF)
        NAMEOFF = bytesToInt(f.read(_LONG))
        DUMMY3 = bytesToInt(f.read(_LONG))
        SIZE = bytesToInt(f.read(_LONG))
        OFFSET = bytesToInt(f.read(_LONG))
        CRC = bytesToInt(f.read(_LONG))
        INFO_OFF = f.tell()

        NAMEOFF += NAME_OFF
        f.seek(NAMEOFF)
        NAME = bytesToString(f.read(_STRLEN))

        print("reading and writing "+source+"/"+NAME)
        f.seek(OFFSET)
        readed = f.read(SIZE)
        df = open(dest+"/"+NAME,"wb")
        df.write(readed)
        df.close()




extractRKV2(fichier,dest)
