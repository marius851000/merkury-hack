import codecs
from struct import *


def bytesToInt(bits):
    return int.from_bytes(bits,byteorder="little")

def bytesToIntBig(bits):
    return int.from_bytes(bits,byteorder="big")


def bytesToString(bits):
    bit2 = b''
    cont = True
    for loop in bits:
        loop = bytes([loop])
        if loop != b"\x00" and cont:
            bit2 += loop
        else:
            cont = False

    return bit2.decode("ASCII")
