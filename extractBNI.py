
from merkurytool import bytesToIntBig, bytesToInt, bytesToString
source = "data/music.ini.bni"
sources = ["controls.ini.bni","music.ini.bni","levels.ini.bni"]
def readBNI(source):
    tagData = """<?xml version="1.0" encoding="UTF-8"?>
    <wxHexEditor_XML_TAG>
      <filename path="/home/marius/roms/hackMerkury/tag.tags">"""
    render = ""
    tagid = 0
    f = open(source,"rb")
    lenght = len(f.read())
    f.close()

    f = open(source,"rb")

    headerSize = 0x44

    originalPath = bytesToString(f.read(32))
    magic = f.read(4)
    assert magic == b"\x00"*3+b"\x64"
    nbElem = bytesToIntBig(f.read(4))
    # end of header, remember to add headerSize to absolute path


    # based on LOS_EN_BNI_3.rkv

    f.seek(48)
    binPool = bytesToIntBig(f.read(4))+headerSize

    # get the string part start and end
    f.seek(0x2C)
    stringStart = bytesToIntBig(f.read(4))+headerSize
    f.seek(0x34)
    stringEnd = bytesToIntBig(f.read(4))+headerSize
    stringSize = stringEnd-stringStart

    f.seek(stringStart)
    stringSection = f.read(stringSize)

    # the script read the text and put it in the first memory file
    # it add an end of the string character
    # the offset at the beggining of the file is called start
    # the "dragon" variable is set to 0
    # the third memory file is from the beggining off the bni to the end of the string
    f.seek(0x30)
    wed = bytesToIntBig(f.read(4))+headerSize # name from the bms script, appear to be an "unknow binary pool"
    # offset of an area with multiple pair of 1 byte ( maybe many possible ), 0 separated
    sed = wed # idem, from the script

    # on line 36, decode string
    f.seek(stringStart)
    names = []
    namesOff = {}
    while f.tell() < stringEnd:

        lastpos = f.tell()

        # it read a string up to the nul chara
        name = b""
        NulReached = False
        while not NulReached:
            chara = f.read(1)
            if chara == b"":
                raise
            if chara == b"\x00":
                NulReached = True
            else:
                name += chara
        #print(bytesToString(name))


        # Padding 4 = on met au multiple de 4 superieur, ici relatif au debut de la partie chara
        # first, compute the padding relitively to the string section start
        # at the end because there is no DO .. WHILE .. in python
        if (f.tell()-stringStart)%4 == 0:
            paddingStringStart = (f.tell()-stringStart)//4*4
        else:
            paddingStringStart = (f.tell()-stringStart)//4*4 + 4
        f.seek(paddingStringStart+stringStart)

        names.append(bytesToString(name))
        namesOff[lastpos] = bytesToString(name)
        tagData += "<TAG id=\""+str(tagid)+"\">\n"
        tagid += 1
        tagData += "<start_offset>"+str(lastpos)+"</start_offset>\n"
        tagData += "<end_offset>"+str(lastpos+len(name)-1)+"</end_offset>\n"
        tagData += "<tag_text>string n°"+str(len(names))+"</tag_text>\n"
        tagData += "<font_colour>#232627</font_colour>\n"
        tagData += "<note_colour>#0C0C00</note_colour>\n"
        tagData += "</TAG>"

    #print(names)

    # parse unknow pool
    # in unknow pool 3, value appear to be unique
    # also unique in the two, at the exception of \xff\xff
    # they appear to be two bit long
    # not in music.ini.bni
    # 0xffff : maybe nul or infinite. There are floating number in the string of music.ini.bni

    # parse unk pool 1
    # TODO:
    #f.seek(binPool)
    #unknowPool1 = []
    #for loop in range((binPool-stringStart)/2):
    #    print("ok")


    # two bite long for unknow pool 1

    #line 52
    f.seek(0x4c)
    items = bytesToIntBig(f.read(2))-1

    f.seek(0x44) # end of the header
    ttt = f.tell()

    # line 58
    result = ""
    finished = False
    dragontests = {}
    loopnb = 0
    maxNB = 0
    bdat = {}
    for nbe in range(nbElem):
        loopnb += 1
        print("---")
        print("loop",loopnb)
        print("pos",ttt)
        f.seek(ttt)
        categoryBin = f.read(2)
        # there are sometimes 0x0002 node
        if categoryBin == b"\x00\x00":
            category = "catDef"
        elif categoryBin == b"\xff\xff":
            category = "def"
        elif categoryBin == b"\x00\x01":
            category = "pair"
        elif categoryBin == b"\x00\x02": # maybe those number is used to caracterize the depth
            category = "strange"
        else:
            print("warning : unknow category")
            category = "unknow"
            #raise


        f.seek(ttt+0x2*4)
        dragontest = bytesToIntBig(f.read(2))
        f.seek(ttt)

        if category == "catDef":
            print("catDef")

            f.seek(ttt+0x2*1)
            pos1 = bytesToIntBig(f.read(2)) # TODO

            f.seek(ttt+0x2*2)
            pos2 = bytesToIntBig(f.read(2)) # TODO not always FF, see other file
            if pos2 == 0xff*0xff+0xff*2:
                name = namesOff[pos1*0x4+stringStart]
            else:
                name = namesOff[pos2*0x4+stringStart]

            f.seek(ttt+0x2*5)
            assert f.read(6) == b"\xff"*2+b"\x8c\xfd\x12\x00" # 3/8 of constant

            f.seek(ttt+0x2*3)
            unk3 = bytesToIntBig(f.read(2)) # TODO
            bdat[unk3] = True

            # dragontest store the linked def, except the last
            dragontests[dragontest] = {"name":name}

        elif category == "def":
            assert dragontest == 0x0
            if loopnb in dragontests:
                render += "["+dragontests[loopnb]["name"]+"]\n"
            else:
                print("warning - not in dragontests")
            print("def")
            # they are always of FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00
            f.seek(ttt)
            assert f.read(0x10) == b"\xff"*2+b"\x00"*14
        elif category == "pair":
            print("pair")
            #assert dragontest == 0xffff
            # false in levels.ini.bni
            f.seek(ttt+2)
            assert f.read(2) == b"\xff"*2 # constant value
            f.seek(ttt+6)
            pairNB = bytesToIntBig(f.read(2)) # the number of pair # il y a autant de pairNB_max*2+1 = len(wed)
            print("pairNB",pairNB)
            f.seek(binPool+(pairNB)*2)
            binpoolData = bytesToIntBig(f.read(2))
            print(binpoolData)
            value = namesOff[binpoolData*0x4+stringStart] # semble correspondre a l'offset des chaine de charactére
            # les même éléments revienne, mais ils sont peu nombreux
            if binpoolData > maxNB:
                maxNB = binpoolData


            f.seek(ttt+0x2*5)
            unk3 = f.read(2)
            assert f.read(4) == b"\x8c\xfd\x12\x00" # dragontest is just before, so, that made the last half unused

            f.seek(ttt+0x2*2)
            paramOff = bytesToIntBig(f.read(2)) # TODO maybe a ref to the last unk pool after string
            param = namesOff[paramOff*0x4+stringStart]
            render += param + " = " + value + "\n"
            # the max unk number appear to be the nb of elem in string pool + pool1 + pool2, without ffff value

        elif category == "strange": # TODO: figure the use
            print("warnign : strange")
            f.seek(ttt+0x2)
            assert f.read(2) == b"\xff\xff"
            unk1 = bytesToIntBig(f.read(2))

            unk2 = bytesToIntBig(f.read(2)) # maybe a link to a second pool ?

            assert f.read(2) == b"\xff"*2

            unk3 = bytesToIntBig(f.read(2))

            assert f.read(4) == b"\x8c\xfd\x12\x00"


        else:
            render += "unknow thing of category : "+category+"\n"

        tagData += "<TAG id=\""+str(tagid)+"\">\n"
        tagid += 1
        tagData += "<start_offset>"+str(ttt)+"</start_offset>\n"
        tagData += "<end_offset>"+str(ttt+0x10-1)+"</end_offset>\n"
        tagData += "<tag_text>"+category+" n°"+str(loopnb)+"</tag_text>\n"
        tagData += "<font_colour>#232627</font_colour>\n"
        if category == "pair":
            color = "FF0000"
        elif category == "catDef":
            color = "00FF00"
        elif category == "def":
            color = "0000FF"
        tagData += "<note_colour>#"+color+"</note_colour>\n"
        tagData += "</TAG>"

        ttt += 0x10

    assert ttt == sed
    tagData += """
</filename>
</wxHexEditor_XML_TAG>"""
    print(names) # maybe someting is placed after names ( for short ? )
    print(bdat)
    print(maxNB)
    print(namesOff)
    a = open("tag.tags","w")
    a.write(tagData)
    a.close()
    print(render)

#print(bytesToIntBig(b"\x00\x1A"))
readBNI(source)
