import os
import lzf


def BTI(bytet):
    rendu = int(bytet.hex(),16)
    return rendu

def BTR(bytet,split=0):
    rendu=bytet.decode("ascii")
    rendu = rendu.rstrip(' \t\r\n\0')
    return rendu

def RTB(string,long):
    rendu = string.encode("ASCII")
    if len(rendu)<long:
        rendu = rendu + bytes(long-len(rendu))
    return rendu

def ITB(nb,long):
    temp = {"0":0,
            "1":1,
            "2":2,
            "3":3,
            "4":4,
            "5":5,
            "6":6,
            "7":7,
            "8":8,
            "9":9,
            "a":10,
            "b":11,
            "c":12,
            "d":13,
            "e":14,
            "f":15}
    rendu = hex(nb)
    rendu = rendu[2:len(rendu)]
    rendu = "0"*(long*2-len(rendu))+rendu
    rendu2 = [0]*(long)
    for loop in range(int(len(rendu)/2)):
        nombre = rendu[(long*2-loop*2)-1]
        nombre2 = rendu[(long*2-loop*2)-2]
        rendu2[long-loop-1] = temp[nombre2]*16+temp[nombre]
    rendu3 = bytes(rendu2)
    return rendu3

def extractResKromeV(locFile,where):
    try:
        os.mkdir(where)
    except:
        print("folder already existing")
    #def
    long=4
    longlong=8
    bit=1
    #script
    fichier = open(locFile,"rb")

    nom = fichier.read(0x40)
    fichier.read(long)
    info_offset = BTI(fichier.read(long))
    fichier.read(long)

    fichierNum = BTI(fichier.read(long))

    #fichier.close()
    #fichier = open(locFile,"rb")
    #fichier.read(info_offset)
    fichier.seek(info_offset)
    for loop in range(fichierNum):
        fichierNom = BTR(fichier.read(0x40))
        print(fichierNom)
        fichierTimespand = fichier.read(longlong)
        fichierZero = fichier.read(long)
        fichierOffset = BTI(fichier.read(long))
        fichierCrc = fichier.read(long)
        fichierSize = BTI(fichier.read(long))
        fichierZSize = BTI(fichier.read(long))
        fichierZero = fichier.read(long)

        tmp = fichier.tell()#save pos


        fichier.seek(fichierOffset)
        if fichierSize == fichierZSize:
            fichier2 = open(where+"/"+fichierNom,"wb")
            fichier2.write(fichier.read(fichierSize))
            fichier2.close()
        else:
            fichierType = BTI(fichier.read(bit))
            if fichierType == 2:
                fichierZSize = fichierZSize - 1
                fichier2 = open(where+"/"+fichierNom,"wb")
                texte=lzf.decompress(fichier.read(fichierZSize),fichierSize)
                if texte==None:
                    print("methode compression insuporte")
                    fichier2.close()
                else:
                    fichier2.write(texte)
                    fichier2.close()

            else:
                print("methode de compression inconnue : " + str(fichierType))
        fichier.seek(tmp)
        #fichier=open(locFile,"rb")#reput pos
		#fichier.read(tmp)

def remadeResKromeV(folder,where,nom):
    #def
    long=4
    longlong=8
    bit=1
    #recompile

    listeFichier = os.listdir(folder)

    rendu = b""

    nom = RTB(nom,0x40)
    dummy=bytes(long)
    info_offset=bytes(long)
    dummy2=bytes(long)
    fichierNum=bytes([len(listeFichier)])
    fo = []
    fs = []

    rendu = nom+dummy+info_offset+dummy2+fichierNum
    for loop in range(len(listeFichier)):
        fo.append(len(rendu))
        fichier2=open(folder+"/"+listeFichier[loop],"rb")
        texte=fichier2.read()
        fs.append(len(texte))
        print(listeFichier[loop])
        rendu = rendu + texte
        fichier2.close()

    info_offset = len(rendu)
    for loop in range(len(listeFichier)):
        fichierNom = RTB(listeFichier[loop],0x40)
        fichierTimespand = bytes(longlong)
        fichierZero = bytes(long)
        fichierOffset = ITB(fo[loop],long)
        fichierCrc = bytes(long)
        fichierSize = ITB(fs[loop],long)
        fichierZSize = fichierSize
        fichierZero = bytes(long)
        rendu = rendu + fichierNom + fichierTimespand + fichierZero + fichierOffset + fichierSize + fichierZSize + fichierZero

    fichier = open(where,"wb")
    fichier.write(rendu[0:0x40+4]+ITB(info_offset,long)+rendu[0x40+8:len(rendu)])
    fichier.close()



if __name__=="__main__":
    #for loop in os.listdir():
    #    if loop.split(".")[len(loop.split("."))-1]=="rkv":
    #        extractResKromeV(loop,"folder")
    #extractResKromeV("mk3resources_wii.rkv","folder")
    #remadeResKromeV("folder","mk3resources_wiimod.rkv","MK3Resources_wii.rkv")
    #print("====================================decoding")
    extractResKromeV("common_wii.rkv","common_wii")
